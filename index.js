// console.log("Hi")

// IF, ELSE IF, ELSE STATEMENT
let numA = -1;
	
	// if statement
		/* Syntax:
			if(condition){
				statement;
			}  
		*/
	
		if(numA<0){
			console.log("Hello")
		}
		console.log(numA<0);

		// It will not print Hello again if statement is false

		numA = 0;

		if (numA<0){
			console.log("Hello again");		
		}
		console.log(numA<0);

		// other example

		let city = "New York";
		if (city === "New York"){
			console.log("Welcome to New York!");
		}



	// else if statement

		let numH = 1;

		if (numH < 0){
			console.log("Hello from numH");
		}
		else if(numH > 0){
			console.log("Hi I'm H!");
		}

		// if the if() is already met, it will no longer evaluate the else if condition

		if (numH > 0){
			console.log("Hello from numH")
		}
		else if(numH === 1){
			console.log("Hi I'm second condition met")
		}

		// Another example
		city = "Tokyo"

		if (city === "New York"){
			console.log("Welcome to New York!");
		}
		else if(city === "Tokyo"){
			console.log("Welcome to Tokyo, Japan!");
		}


	// else statement

		numH = 2;

		if(numH < 0){
			console.log("Hello I'm numH");
		}
		else if(numH > 2){
			console.log("numH is greater than 2");
		}
		else if(numH > 3){
			console.log("numH is greater than 3");
		}
		else{
			console.log("numH from else");
		}


// CONDITIONAL STATEMENT WITH FUNCTIONS

		let message;

		function detTyphoonIn(windSpeed){
			if(windSpeed < 0){
				return "Invalid Argument";
			}
			else if(windSpeed > 0 && windSpeed < 31){
				return "Not a typoon yet.";
			}
			else if(windSpeed <= 60){
				return "Tropical depression detected";
			}
			else if(windSpeed >= 61 && windSpeed <= 88){
				return "Tropical Storm detected";
			}
			else if(windSpeed >= 89 && windSpeed <= 117){
				return "Severe Tropical Storm detected";
			}
			else{
				return "Typhoon detected"
			}
		}

		console.log(detTyphoonIn(-3));
		// Return the string to the variable message that invoked
		message = detTyphoonIn(123);
		console.log(message);

		// console.warn()

		if(message === "Typhoon detected") {
			console.warn(message)
		}


// TRUTHY & FALSY

		/* "truthy" value - is a  value that is considered true when encountered in a Boolean context
			    true
			    {}
			    []
			    42
			    "0"
			    "false"
			    new Date()
			    -42
			    12n
			    3.14
			    -3.14
			    Infinity
			    -Infinity

			"Falsy" value - exceptions for truthy:
			1. false
	        2. 0
	        3. -0
	        4. ""
	        5. null
	        6. undefined
	        7. NaN - not a number
		*/

	// Truthy Examples

		if(true){
			console.log("Truthy");
		}

		if(1){
			console.log("Truthy");
		}

		if([]){
			console.log("Truthy");
		}

	// Falsy Examples
		if(false){
			console.log("Falsy");
		}

		if(0){
			console.log("Falsy");
		}

		if(undefined){
			console.log("Falsy");
		}


// CONDITIONAL TERNARY OPERATOR

	/* alternative to an "if else" statement; have an 	
		implicit "return" statement, meaning w/o return keyword.
		syntax: (expression) ? ifTrue : ifFalse;
	*/

	// let ternaryResult = (1 < 18) ? true : false;
	let ternaryResult = (1 < 18) ? 1 : 2;
	console.log("Result of Ternary Operator: " + ternaryResult );

	// Multiple Statement execution
	let name;
	function isLegalAge(){
		name="John";
		return "You are of the legal age";
	}
	function isUnderAge(){
		name = "Jane";
		return "You are under age!"
	}

	let age = parseInt(prompt("What is you age?"));
		// the parseInt() funtion converts the input receive into a number data type
	console.log(age);

	let legalAge = (age >= 18) ? isLegalAge() : isUnderAge();

	console.log("Result of Ternary Operator in Functions: " + legalAge + ", " + name) 


// SWITCH STATEMENT

	/*
	Syntax:
	switch (expression){
		case value:
			statement;
			break;
		dafault:
			statement;
			break;
	}
	*/

	let day = prompt("What day of the week is it today?").toLowerCase();
	console.log(day);

	switch (day){
		case "monday" :
			console.log("The color of the day is red!");
			break;
		case "tuesday" :
			console.log("The color of the day is orange!");
			break;
		case "wednesday":
			console.log("The color of the day is yellow!");
			break;
		case "thursday":
			console.log("The color of the day is green!");
			break;
		case "friday":
			console.log("The color of the day is blue!");
			break;
		case "saturday":
			console.log("The color of the day is indigo!");
			break;
		case "sunday":
			console.log("The color of the day is violet!");
			break;
		default:
			console.log("Please input a valid day.");
			break;
	}


// TRY-CATCH-FINALLY STATEMENT	

	function showIntensityAlert(windSpeed){
		try{
			alerat(detTyphoonIn(windSpeed))
		}
		catch(error){
			console.warn(error.message);
		}
		finally{
			alert("Intensity updates will show alert");
		}
	}

	showIntensityAlert(110)